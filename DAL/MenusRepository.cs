﻿using DAL.Helper;
using DocumentFormat.OpenXml.Office.CustomUI;
using Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL
{
    public partial class MenusRepository : ICustomerRepository
    {
        private IDatabaseHelper _dbHelper;

        public MenusRepository(IDatabaseHelper dbHelper)
        {
            _dbHelper = dbHelper;
        }

        public bool Create(Menu model)
        {
            string msgError = "";
            try
            {
                var result = _dbHelper.ExecuteScalarSProcedureWithTransaction(out msgError, "sp_menus_create",
                    "@")
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
